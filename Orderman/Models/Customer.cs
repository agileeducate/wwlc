﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Orderman.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public CommunicationType CommunicationType { get; set; }
    }
    public enum CommunicationType { Email, SMS };
}