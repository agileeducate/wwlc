﻿namespace Orderman.Models
{
    public class OrderItemRequest
    {
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public int Qty { get; set; }
    }
}