﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Orderman.Models
{
    public class Account
    {
        public double Balance { get; set; }
        public int CustomerId { get; set; }
        public double Limit { get; set; }
        public bool MayExceedLimitOnOrder { get; set; }
        public AccountStatus State { get; set; }
    }
    public enum AccountStatus { PreValidation, Open, Blocked, Closed };
}