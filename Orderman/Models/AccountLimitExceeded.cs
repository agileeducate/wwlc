﻿using Asbm.Sarah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Orderman.Models
{
    public class AccountLimitExceeded:IEntityIdentity
    {
        public double BalanceAfterOrderProcessed { get; set; }
        public int CustomerId { get; set; }
        public int Id { get; set; }
        public double Limit { get; set; }
        public DateTime LimitExceedDate { get; set; }
    }
}