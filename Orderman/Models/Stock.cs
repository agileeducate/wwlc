﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Orderman.Models
{
    public class Stock
    {
        public int ProductId { get; set; }
        public int Reserved { get; set; }
        public int CurrentStock { get; set; }
    }
}