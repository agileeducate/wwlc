﻿using Asbm.Sarah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Orderman.Models
{
    public class Order:IEntityIdentity
    {
        public int CustomerId { get; set; }
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int Qty { get; set; }
    }
}