﻿namespace Orderman.Process
{
    public class ProcessStepResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public ProcessStepResult(string message, bool success)
        {
            Message = message;
            Success = success;
        }
    }
}