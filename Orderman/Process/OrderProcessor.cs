﻿using Asbm.Sarah;
using Orderman.Interfaces;
using Orderman.Models;
using System;
using System.Threading.Tasks;

namespace Orderman.Process
{
    public class OrderProcessor
    {
        private IRepository repo;
        private IGALInterface gal;
        private IMessageBroker messageBroker;
        private Account account = null;
        private Product product = null;
        private Customer customer = null;

        public OrderProcessor(IRepository repo, IGALInterface gal, IMessageBroker messageBroker)
        {
            this.repo = repo;
            this.gal = gal;
            this.messageBroker = messageBroker;
        }

        public async Task<ProcessStepResult> ProcessOrder(int customerId, int productId, int qty)
        {
            ProcessStepResult processStepResult = await ValidateRequest(customerId, productId);
            if (processStepResult.Success)
            {
                processStepResult = ProcessOrder(qty);
            }
            return new ProcessStepResult(processStepResult.Message, processStepResult.Success);
        }

        private async Task<ProcessStepResult> ValidateRequest(int customerId, int productId)
        {
            ProcessStepResult processStepResult = new ProcessStepResult(string.Empty, true);
            customer = await ValidateCustomer(customerId, processStepResult);
            if (processStepResult.Success)
            {
                product = ValidateProduct(productId, processStepResult);
            }
            if (processStepResult.Success)
            {
                account = ValidataAccount(customerId, processStepResult);
            }
            return processStepResult;
        }

        private async Task<Customer> ValidateCustomer(int customerId, ProcessStepResult processStepResult)
        {
            Customer customer = await gal.GetCustomer(customerId, processStepResult);
            if (customer == null)
            {
                ProcessStepFailed(processStepResult, "Customer Not Found");
            }

            return customer;
        }

        private static void ProcessStepFailed(ProcessStepResult processStepResult, string processFailureMessage)
        {
            processStepResult.Success = false;
            processStepResult.Message = processFailureMessage;
        }

        private Product ValidateProduct(int productId, ProcessStepResult processStepResult)
        {
            Product product = repo.Find<Product>(x => x.Id == productId);
            if (product == null)
            {
                ProcessStepFailed(processStepResult, "Product Not Found");
            }

            return product;
        }

        private Account ValidataAccount(int customerId, ProcessStepResult processStepResult)
        {
            Account account = null;
            if (processStepResult.Success)
            {
                account = repo.Find<Account>(x => x.CustomerId == customerId);
                if (account == null)
                {
                    ProcessStepFailed(processStepResult, "Customer Account Not Found");
                }
                else if (account.State == AccountStatus.Blocked)
                {
                    ProcessStepFailed(processStepResult, "Customer Account Are Blocked");
                }
            }

            return account;
        }

        private ProcessStepResult ProcessOrder(int qty)
        {
            ProcessStepResult processStepResult = new ProcessStepResult(string.Empty, true);
            bool exceedLimitNotificationRequired = ExceedLimitNotificationRequired(qty, processStepResult);

            if (processStepResult.Success)
            {
                ReserveStockForOrder(qty, processStepResult);
            }
            if (processStepResult.Success)
            {
                PlaceOrder(qty, processStepResult);
            }
            if (exceedLimitNotificationRequired)
            {
                CreateAccountLimitExceedNotification(qty);
            }
            return processStepResult;
        }

        private bool ExceedLimitNotificationRequired(int qty, ProcessStepResult processStepResult)
        {
            bool exceedLimitNotificationRequired = false;
            if (LimitWillBeExceeded(qty))
            {
                if (!account.MayExceedLimitOnOrder)
                {
                    ProcessStepFailed(processStepResult, "Account Limit will be Exceeded with this Order");
                }
                exceedLimitNotificationRequired = true;
            }

            return exceedLimitNotificationRequired;
        }

        private bool LimitWillBeExceeded(int qty)
        {
            return account.Limit < (account.Balance + (qty * product.Price));
        }

        private void ReserveStockForOrder(int qty, ProcessStepResult processStepResult)
        {
            StockRepository stockReservator = new StockRepository(repo, processStepResult);
            bool reservedStock = stockReservator.ReserveStock(product.Id, qty);
        }

        private void PlaceOrder(int qty, ProcessStepResult processStepResult)
        {
            Order order = new Order() { CustomerId = customer.Id, ProductId = product.Id, Qty = qty };
            repo.AddSeed<Order>(order);
            UpdateStock(qty);
            processStepResult.Success = true;
            processStepResult.Message = String.Format("Order Created {0}", order.Id);            
        }

        private void UpdateStock(int qty)
        {
            StockRepository stockRepo = new Process.StockRepository(repo, null);
            stockRepo.UseReservedStock(qty, product.Id);
        }

        private void CreateAccountLimitExceedNotification(int qty)
        {
            AccountLimitExceeded accountLimitExceeded = new AccountLimitExceeded()
            {
                CustomerId = customer.Id,
                LimitExceedDate = DateTime.Now,
                Limit = account.Limit,
                BalanceAfterOrderProcessed = account.Balance + (qty * product.Price)
            };
            repo.AddSeed<AccountLimitExceeded>(accountLimitExceeded);
            messageBroker.SendMessage(customer.CommunicationType, "Account Limit Exceed Warning", String.Format("After your last order have been processed your account Balance will be exceeded. New Balance: {0} Limit:{1}", accountLimitExceeded.BalanceAfterOrderProcessed, accountLimitExceeded.Limit));
        }
    }
}