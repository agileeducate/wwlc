﻿using Asbm.Sarah;
using Orderman.Models;

namespace Orderman.Process
{
    public class StockRepository
    {
        private IRepository repo = null;
        private ProcessStepResult processStepResult = null;
        public StockRepository(IRepository repo, ProcessStepResult processStepResult)
        {
            this.repo = repo;
            this.processStepResult = processStepResult;
        }
        public bool ReserveStock(int productId, int qty)
        {
            bool reservedStock = repo.Update<Stock>(x => (((x.CurrentStock - x.Reserved) >= qty) && (x.ProductId == productId)),
                x => x.Reserved = x.Reserved + qty) > 0;
            if (!reservedStock)
            {
                processStepResult.Success = false;
                processStepResult.Message = "Low or No Stock for this Product";
            }
            return reservedStock;
        }
        public bool UseReservedStock(int qty, int productId)
        {
            return repo.Update<Stock>(x => (x.ProductId == productId),
            x =>
            {
                x.Reserved = x.Reserved - qty;
                x.CurrentStock = x.CurrentStock - qty;
            }) > 0;
        }
    }
}