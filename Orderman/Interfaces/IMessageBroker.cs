﻿using Orderman.Models;

namespace Orderman.Interfaces
{
    public interface IMessageBroker
    {
        void SendMessage(CommunicationType communicationType, string subject, string body);
    }
}
