﻿using Orderman.Models;
using Orderman.Process;
using System.Threading.Tasks;

namespace Orderman.Interfaces
{
    public interface IGALInterface
    {
        Task<Customer> GetCustomer(int customerId, ProcessStepResult processStepResult);
    }
}
