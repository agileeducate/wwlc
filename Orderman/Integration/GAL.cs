﻿using Orderman.Interfaces;
using Orderman.Models;
using Orderman.Process;
using Orderman.Utilities;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Orderman.Integration
{
    public class GAL: IGALInterface
    {
        public async Task<Customer> GetCustomer(int customerId, ProcessStepResult processStepResult)
        {
            Customer customer = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:58839/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = null;
                try
                {
                    response = await client.GetAsync(String.Format("api/Values/{0}", customerId));
                }
                catch (Exception e)
                {
                    processStepResult.Success = false;
                    processStepResult.Message = "Customer Not Found , Connecting to the Source System Failed";
                }
                if (response.IsSuccessStatusCode)
                {
                    object obj = await response.Content.ReadAsAsync(typeof(Customer));
                    customer = obj as Customer;
                    processStepResult.Success = customer != null;
                }
                else
                {
                    Logging.LogException(response.ReasonPhrase);
                    processStepResult.Success = false;
                    processStepResult.Message = String.Format("Customer Not Found {0}", response.ReasonPhrase);
                }
            }
            return customer;
        }
    }
}