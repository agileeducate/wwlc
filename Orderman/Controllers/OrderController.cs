﻿using Asbm.Sarah;
using Orderman.Integration;
using Orderman.Models;
using Orderman.Process;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Orderman.Controllers
{
    public class OrderController : ApiController
    {
        private IHttpActionResult GetActionResult(bool succeed, string message)
        {
            HttpStatusCode code = succeed ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
            return ResponseMessage(Request.CreateResponse(code,message));
        }

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public async Task<IHttpActionResult> Post([FromBody]OrderItemRequest request)
        {           
            ProcessStepResult processStepResult = new ProcessStepResult(string.Empty, false);
            CachedRepository repo = new CachedRepository(new Cache(), System.Web.HttpContext.Current.Server.MapPath("\\App_Data"));
            OrderProcessor orderProcessor = new OrderProcessor(repo, new GAL(), new MessageBroker());
            processStepResult = await orderProcessor.ProcessOrder(request.CustomerId, request.ProductId, request.Qty);
            return GetActionResult(processStepResult.Success, processStepResult.Message);
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

    }

}
