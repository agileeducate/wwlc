﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace GAL.Web.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public async Task<IEnumerable<Customer>> Get()
        {
            IEnumerable<Customer> customer = await Task.Run(() => GetCustomers(5)); 
            return customer;
        }

        // GET api/values/5
        public async Task<Customer> Get(int id)
        {
            Customer customer = await Task.Run(() => GetCustomer(id)); 
            return customer;
        }

        private Customer GetCustomer(int id)
        {
            Thread.Sleep(id);
            if (id < 100)
            {
                return new Controllers.Customer() { Id = id, Name = "Customer " + id.ToString(), Phone = String.Format("011 900 {0}", id.ToString().PadLeft(4, '0')) };
            }
            else
            {
                return null;
            }
        }

        private IEnumerable<Customer> GetCustomers(int id)
        {
            List<Customer> customers = new List<Customer>();
            for (int i = 0; i < id; i++)
            {
                customers.Add(new Customer() { Id = i, Name = String.Format("My Customer {0}", i), Phone = String.Format("011 900 {0}",i.ToString().PadLeft(4,'0')) });
            }
            Thread.Sleep(id);
            return customers;
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
