﻿namespace GAL.Web.Controllers
{
    public class Customer
    {
        public int Id { get; internal set; }
        public string Name { get; internal set; }
        public string Phone { get; internal set; }
        public CommunicationType CommunicationType { get; set; }
    }
    public enum CommunicationType { Email, SMS };
}