﻿using Orderman.Interfaces;
using System.Threading.Tasks;
using Orderman.Models;
using Orderman.Process;

namespace Orderman.Tests.Mocks
{
    public class MockGAL : IGALInterface
    {
        public async Task<Customer> GetCustomer(int customerId, ProcessStepResult processStepResult)
        {
            processStepResult.Success = true;
            return new Customer()
            {
                Id = customerId,
                Name = string.Format("Mocked Customer {0}", customerId),
                Phone = "0119001000",
                CommunicationType = CommunicationType.Email
            };
        }
        
    }
}
