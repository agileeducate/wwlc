﻿using Orderman.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orderman.Tests.Mocks
{
    public class MockRepoWithStockAndReservedQty:MockRepoWithNoStock
    {
        public MockRepoWithStockAndReservedQty()
        {
            repo.Add<Stock>(new Stock() { ProductId = 3, CurrentStock = 2, Reserved = 1 });
        }
    }
}
