﻿using Orderman.Models;
using System;

namespace Orderman.Tests.Mocks
{
    public class MockForValidOrderScenarioSpy:MockForValidOrderScenario
    {
        public bool OrderAdded { get; private set; }
        public bool StockUpdated { get; private set; }
        public override int AddSeed<T>(T item)
        {
            if (typeof(T).Equals(typeof(Order)))
            {
                OrderAdded = true;
            }
            return base.AddSeed<T>(item);
        }
        public override int Update<T>(Func<T, bool> predicate, Action<T> operation)
        {
            if (typeof(T).Equals(typeof(Stock)))
            {
                StockUpdated = true;
            }
            return base.Update<T>(predicate, operation);
        }
    }
}
