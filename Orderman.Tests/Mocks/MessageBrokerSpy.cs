﻿using Orderman.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orderman.Models;

namespace Orderman.Tests.Mocks
{
    public class MessageBrokerSpy : IMessageBroker
    {
        public int CallCount { get; private set; }
        public string Subject { get; private set; }
        public string Body { get; private set; }
        public void SendMessage(CommunicationType communicationType, string subject, string body)
        {
            CallCount++;
            Subject = subject;
            Body = body;
        }
    }
}
