﻿using Orderman.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orderman.Models;
using Orderman.Process;

namespace Orderman.Tests.Mocks
{
    public class MockGALNoCustomerFound : IGALInterface
    {
        public async Task<Customer> GetCustomer(int customerId, ProcessStepResult processStepResult)
        {
            processStepResult.Success = false;
            processStepResult.Message = "Customer Not Found";
            return null;
        }
    }
}
