﻿using System;
using Orderman.Models;

namespace Orderman.Tests.Mocks
{
    public class MockRepoWithEnoughStock: MockRepoWithNoStock
    {
        Stock stock = null;
        public MockRepoWithEnoughStock()
        {
            stock = new Stock() { ProductId = 30, CurrentStock = 200, Reserved = 12 };
            repo.Add<Stock>(stock);
        }

        internal int ReservedStock()
        {
            return stock.Reserved;
        }
    }
}
