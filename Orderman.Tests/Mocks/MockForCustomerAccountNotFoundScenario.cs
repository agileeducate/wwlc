﻿using Asbm.Sarah;
using Orderman.Models;
using System;
using System.Collections.Generic;

namespace Orderman.Tests.Mocks
{
    public class MockForCustomerAccountNotFoundScenario : MockForValidOrderScenario
    {      

        public override T Find<T>(Func<T, bool> predicate) 
        {
            T result = base.Find<T>(predicate);
            if (typeof(T).Equals(typeof(Account)))
            {
                return null;
            }
            return result;
        }
        
    }
}
