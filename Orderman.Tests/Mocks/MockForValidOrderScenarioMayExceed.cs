﻿using Orderman.Models;
using System;

namespace Orderman.Tests.Mocks
{
    public class MockForValidOrderScenarioMayExceed:MockForValidOrderScenario
    {
        public override T Find<T>(Func<T, bool> predicate)
        {
            T result = base.Find<T>(predicate);
            if (typeof(T).Equals(typeof(Account)))
            {
                Account acc = result as Account;
                acc.MayExceedLimitOnOrder = true;
            }
            return result;
        }
    }
}
