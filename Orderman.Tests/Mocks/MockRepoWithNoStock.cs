﻿using System;
using System.Collections.Generic;
using Asbm.Sarah;
using Orderman.Models;

namespace Orderman.Tests.Mocks
{
    public class MockRepoWithNoStock : IRepository
    {
        protected CachedRepository repo = new CachedRepository(new Cache());
        public MockRepoWithNoStock()
        {
            repo.Add<Stock>(new Stock() { ProductId = 1, CurrentStock = 0, Reserved = 0 });
        }
        public void Add<T>(T item) where T : class
        {
            throw new NotImplementedException();
        }

        public void AddUnpersisted<T>(T item) where T : class
        {
            throw new NotImplementedException();
        }

        public int Delete<T>(Func<T, bool> predicate) where T : class
        {
            return repo.Delete<T>(predicate);
        }

        public T Find<T>(Func<T, bool> predicate) where T : class
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> FindAll<T>(Func<T, bool> predicate) where T : class
        {
            throw new NotImplementedException();
        }

        public T FindFirstItem<T>(Func<T, T, bool> predicate, T item) where T : class
        {
            throw new NotImplementedException();
        }

        public int Update<T>(Func<T, bool> predicate, Action<T> operation) where T : class
        {
            return repo.Update<T>(predicate, operation);
        }

        int IRepository.AddSeed<T>(T item)
        {
            throw new NotImplementedException();
        }
    }
}