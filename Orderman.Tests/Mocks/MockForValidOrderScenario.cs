﻿using Asbm.Sarah;
using Orderman.Models;
using System;
using System.Collections.Generic;

namespace Orderman.Tests.Mocks
{
    public class MockForValidOrderScenario : IRepository
    {
        public void Add<T>(T item) where T : class
        {
            throw new NotImplementedException();
        }

        public void AddUnpersisted<T>(T item) where T : class
        {
            throw new NotImplementedException();
        }

        public int Delete<T>(Func<T, bool> predicate) where T : class
        {
            throw new NotImplementedException();
        }

        public virtual T Find<T>(Func<T, bool> predicate) where T : class
        {
            if (typeof(T).Equals(typeof(Account)))
            {
                return new Account() { CustomerId = 1, Balance = 0, Limit = 10000, MayExceedLimitOnOrder = false, State = AccountStatus.Open } as T;
            }
            else if (typeof(T).Equals(typeof(Product)))
            {
                return new Product() { Id = 1, Price = 1F } as T;
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<T> FindAll<T>(Func<T, bool> predicate) where T : class
        {
            throw new NotImplementedException();
        }

        public T FindFirstItem<T>(Func<T, T, bool> predicate, T item) where T : class
        {
            throw new NotImplementedException();
        }

        public virtual int Update<T>(Func<T, bool> predicate, Action<T> operation) where T : class
        {
            return 1;
        }

        public virtual int AddSeed<T>(T item) where T : class, IEntityIdentity
        {
            return 0;
        }
    }
}
