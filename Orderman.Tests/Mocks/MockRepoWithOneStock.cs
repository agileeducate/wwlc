﻿using Orderman.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orderman.Tests.Mocks
{
    public class MockRepoWithOneStock: MockRepoWithNoStock
    {
        public MockRepoWithOneStock()
        {
            repo.Add<Stock>(new Stock() { ProductId = 2, CurrentStock = 1, Reserved = 0 });
        }
    }
}
