﻿using Orderman.Models;
using System;

namespace Orderman.Tests.Mocks
{
    public class MockForProductNotFoundScenario: MockForValidOrderScenario
    {
        public override T Find<T>(Func<T, bool> predicate)
        {
            T result = base.Find<T>(predicate);
            if (typeof(T).Equals(typeof(Product)))
            {
                return null;
            }
            return result;
        }
    }
}
