﻿using Asbm.Sarah;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orderman.Models;
using Orderman.Process;
using Orderman.Tests.Mocks;

namespace Orderman.Tests
{
    [TestClass]
    public class StockReservatorTests
    {
        private void CleanUp()
        {
            MockRepoWithNoStock repo = new MockRepoWithNoStock();
            repo.Delete<Stock>(x=>true);
        }
        [TestMethod]
        public void Test_Given_Stock_Level_Zero_StockReservator_Returns_False_On_ReserveStock_Of_Qty_Of_One()
        {
            StockRepository stockReservator = new StockRepository(new MockRepoWithNoStock(), new ProcessStepResult(string.Empty, true));
            bool result = stockReservator.ReserveStock(1, 1);
            Assert.IsFalse(result);         
        }
        [TestMethod]
        public void Test_Given_Stock_Level_One_StockReservator_Returns_true_On_ReserveStock_Of_Qty_Of_One()
        {
            IRepository repo = new MockRepoWithOneStock();
            StockRepository stockReservator = new StockRepository(repo, new ProcessStepResult(string.Empty, true));
            bool result = stockReservator.ReserveStock(2, 1);
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void Test_Given_Stock_Level_Two_And_Reserved_One_StockReservator_Returns_true_On_ReserveStock_Of_Qty_Of_One()
        {
            IRepository repo = new MockRepoWithStockAndReservedQty();
            StockRepository stockReservator = new StockRepository(repo, new ProcessStepResult(string.Empty, true));
            bool result = stockReservator.ReserveStock(3, 1);
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void Test_Given_Stock_Level_Two_And_Reserved_One_StockReservator_Returns_false_On_ReserveStock_Of_Qty_Of_Five()
        {
            IRepository repo = new MockRepoWithStockAndReservedQty();
            ProcessStepResult processResult = new ProcessStepResult(string.Empty, true);
            StockRepository stockReservator = new StockRepository(repo, processResult);
            bool result = stockReservator.ReserveStock(3, 5);
            Assert.IsFalse(result);
            Assert.IsFalse(processResult.Success);
            Assert.IsTrue(processResult.Message.Contains("Low or No Stock for this Product"));
        }
        [TestMethod]
        public void Test_Given_No_Stock_Use_Reserved_Stock_Returns_False()
        {
            IRepository repo = new MockRepoWithNoStock();
            StockRepository stockReservator = new StockRepository(repo, null);
            bool useReservedStock = stockReservator.UseReservedStock(1, 10);
            Assert.IsFalse(useReservedStock);
        }
        [TestMethod]
        public void Test_Given_Enough_Stock_Use_Reserved_Stock_Returns_True()
        {
            MockRepoWithEnoughStock repo = new MockRepoWithEnoughStock();
            StockRepository stockReservator = new StockRepository(repo, null);
            bool useReservedStock = stockReservator.UseReservedStock(10, 30);
            Assert.IsTrue(useReservedStock);
            Assert.AreEqual(2, repo.ReservedStock());
            CleanUp();
        }
    }
}
