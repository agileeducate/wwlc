﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Orderman.Tests
{
    [TestClass]
    public class SanityTest
    {
        [TestMethod]
        public void TestSuccessFullOrderWithValidCustomerAccountAndProductWithStock()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59251/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = null;
                try
                {
                    response = client.PostAsJsonAsync("api/Order/", new { CustomerId = 1, ProductId = 1, Qty = 3 }).Result;
                }
                catch
                {
                    Assert.Fail();
                }
                Assert.IsTrue(response.IsSuccessStatusCode);                   
            }
        }
        [TestMethod]
        public void TestFailedOrderWithValidCustomerAccountAndProductWithLowStock()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59251/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = null;
                response = client.PostAsJsonAsync("api/Order/", new { CustomerId = 1, ProductId = 1, Qty = int.MaxValue }).Result;
                string body = response.Content.ReadAsStringAsync().Result;
                Assert.IsFalse(response.IsSuccessStatusCode);
                Assert.IsTrue(body.Contains("Low or No Stock for this Product"));                
            }
        }        
    }
}
