﻿using Asbm.Sarah;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orderman.Tests.Mocks;
using Orderman.Interfaces;
using Orderman.Process;
using Orderman.Models;

namespace Orderman.Tests
{
    [TestClass]
    public class OrderProcessTests
    {
        private IGALInterface gal = new MockGAL();
        private IMessageBroker messageBroker = new MessageBroker();  
        [TestMethod]
        public void Test_That_Given_A_Valid_Order_Scenario_Returns_Succeed_True()
        {
            IRepository repo = new MockForValidOrderScenario();
            OrderProcessor orderProcessing = new OrderProcessor(repo, gal, messageBroker);
            ProcessStepResult result = orderProcessing.ProcessOrder(1, 1, 1).Result;
            Assert.IsTrue(result.Success);
        }
        [TestMethod]
        public void Test_That_Given_A_Customer_Is_Not_Found_Returns_Success_False_Message_Customer_Not_Found()
        {
            IRepository repo = new MockForValidOrderScenario();
            IGALInterface localGal = new MockGALNoCustomerFound();
            OrderProcessor orderProcessing = new OrderProcessor(repo, localGal, messageBroker);
            ProcessStepResult result = orderProcessing.ProcessOrder(1, 1, 1).Result;
            Assert.IsFalse(result.Success);
            Assert.IsTrue(result.Message.Contains("Customer Not Found"));
        }
        [TestMethod]
        public void Test_That_Given_A_Customer_Account_Is_Not_Found_Returns_Success_False_Message_Customer_Account_Not_Found()
        {
            IRepository repo = new MockForCustomerAccountNotFoundScenario();
            OrderProcessor orderProcessing = new OrderProcessor(repo, gal, messageBroker);
            ProcessStepResult result = orderProcessing.ProcessOrder(1, 1, 1).Result;
            Assert.IsFalse(result.Success);
            Assert.IsTrue(result.Message.Contains("Customer Account Not Found"));
        }
        [TestMethod]
        public void Test_That_Given_A_Customer_Blocked_Account_Is_Found_Returns_Success_False_Message_Customer_Account_Not_Found()
        {
            IRepository repo = new MockForBlockCustomerAccountFoundScenario();
            OrderProcessor orderProcessing = new OrderProcessor(repo, gal, messageBroker);
            ProcessStepResult result = orderProcessing.ProcessOrder(1, 1, 1).Result;
            Assert.IsFalse(result.Success);
            Assert.IsTrue(result.Message.Contains("Customer Account Are Blocked"));
        }
        [TestMethod]
        public void Test_That_Given_A_Product_Is_Not_Found_Returns_Success_False_Message_Product_Not_Found()
        {
            IRepository repo = new MockForProductNotFoundScenario();
            OrderProcessor orderProcessing = new OrderProcessor(repo, gal, messageBroker);
            ProcessStepResult result = orderProcessing.ProcessOrder(1, 1, 1).Result;
            Assert.IsFalse(result.Success);
            Assert.IsTrue(result.Message.Contains("Product Not Found"));
        }
        [TestMethod]
        public void Test_That_Given_A_Account_Limit_Will_Be_Exceeded_And_May_Not_Be_Exceeded_Returns_Success_False_Message_Account_Limit_Exceed_Message()
        {
            IRepository repo = new MockForValidOrderScenario();
            OrderProcessor orderProcessing = new OrderProcessor(repo, gal, messageBroker);
            ProcessStepResult result = orderProcessing.ProcessOrder(1, 1, 100000).Result;
            Assert.IsFalse(result.Success);
            Assert.IsTrue(result.Message.Contains("Account Limit will be Exceeded with this Order"));
        }
        [TestMethod]
        public void Test_That_Given_A_Account_Limit_Will_Be_Exceeded_And_May_Exceed_Returns_Success_True_With_Message_Send()
        {
            IRepository repo = new MockForValidOrderScenarioMayExceed();
            MessageBrokerSpy spy = new MessageBrokerSpy();
            OrderProcessor orderProcessing = new OrderProcessor(repo, gal, spy);
            ProcessStepResult result = orderProcessing.ProcessOrder(1, 1, 100000).Result;
            Assert.IsTrue(result.Success);
            Assert.AreEqual(1, spy.CallCount);
            Assert.AreEqual("Account Limit Exceed Warning", spy.Subject);
            Assert.IsTrue(spy.Body.Contains("After your last order have been processed your account Balance will be exceeded."));
        }
        [TestMethod]
        public void Test_That_Given_A_Valid_Order_Scenario_Adds_Order()
        {
            MockForValidOrderScenarioSpy repo = RunValidOrderScenarioWithSpy();
            Assert.IsTrue(repo.OrderAdded);
        }

        [TestMethod]
        public void Test_That_Given_A_Valid_Order_Scenario_Updates_Stock()
        {
            MockForValidOrderScenarioSpy repo = RunValidOrderScenarioWithSpy();
            Assert.IsTrue(repo.StockUpdated);
        }

        private MockForValidOrderScenarioSpy RunValidOrderScenarioWithSpy()
        {
            MockForValidOrderScenarioSpy repo = new MockForValidOrderScenarioSpy();
            OrderProcessor orderProcessing = new OrderProcessor(repo, gal, messageBroker);
            ProcessStepResult result = orderProcessing.ProcessOrder(1, 1, 1).Result;
            return repo;
        }
    }
}
